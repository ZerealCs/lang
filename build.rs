extern crate peg;


fn main() {
    peg::cargo_build("src/compiler/grammar.rustpeg");
    peg::cargo_build("src/prompt/grammar_prompt.rustpeg");
}
