# Lang

# Build

Install rust compiler
> curl https://sh.rustup.rs -sSf | sh

Install dependencies and build
> cargo build

Run
> cargo run


# Notes

Ejemplo de guía de un modulo rest:
```typescript
module github : rest {
  getToken = {
    auth: {
      url: "https://api.github.com/authorizations",
      type: "Basic",
      body: '
        {
          "scopes": ["user", "repo", "notifications", "gist"],
          "note": "lang Rust"
        }"
      '
    },
    retrieve: {
      token: "token"
    }
  }

  request = {
    header = {
      user_agent: "GHScript"
    }
    url_base = "https://api.github.com/"
  }

  with /user {
    GET /repos(visibility, affiliation, type, sort, direction) {

    }
  }

  with /users {
    with /:username {
      extend /user
    }
  }


  with /repos {
    with /:owner/:repo {
      GET /projects {

      }

      GET & PUT /topics {

      }

      GET /contributors {

      }

      GET /languages {

      }

      GET /teams {

      }

      GET /tags {

      }

      DELETE {

      }

      // https://developer.github.com/v3/repos/#input-3
      POST /transfer(new_owner, team_id) {

      }
    }
  }
}
```
