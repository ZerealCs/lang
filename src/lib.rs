//#![feature(trace_macros)]
#![feature(entry_and_modify)]
extern crate futures;
extern crate hyper;
extern crate hyper_tls;
extern crate tokio_core;
extern crate termion;
extern crate serde;
extern crate serde_json;
extern crate serde_yaml;
extern crate xdg;
#[macro_use]
extern crate maplit;
//#[macro_use]
//extern crate serde_derive;
#[macro_use]
extern crate clap;
extern crate intmap;


pub mod compiler;
pub mod reftable;
pub mod auth;
pub mod config;
pub mod prompt;
pub mod module;
pub mod fileref;
pub mod interpreter;
pub mod error;
pub mod value;
pub mod objects;
pub mod reporter;

use std::fs::File;
use std::io::{BufReader, Read, Write, stdin, stdout};
use std::path::PathBuf;
use std::process;

use termion::event::{Event, Key};
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use termion::style;

use config::Config;
use fileref::FileRef;
use interpreter::Interpreter;

pub fn start(mut config: &mut Config, opt_filepath: Option<&str>, verbose: bool) {
    let mut interpreter = Interpreter::new(&mut config, verbose);
    if let Some(filepath) = opt_filepath {
        compile(&mut interpreter, filepath)
    } else {
        start_repl(&mut interpreter)
    }
}

pub fn compile(interpreter: &mut Interpreter, filepath: &str) {
    let mut file_contents = String::new();
    File::open(filepath)
        .and_then(|file| {
            BufReader::new(file).read_to_string(&mut file_contents)
        })
        .expect("Open and read file");
    // REVIEW: Fix compile method
    let fileref = FileRef {
        path_file: PathBuf::from(filepath),
        contents: file_contents,
    };
    let _ = interpreter.compile(&fileref);
}


/// Initialize intepreter
pub fn start_repl(interpreter: &mut Interpreter) {
    let stdout = stdout();
    let mut stdout = stdout.lock();
    let stdin = stdin();
    let mut stdin = stdin.lock();
    println!("Welcome to REPL");

    // REPL Loop
    loop {
        print!("{}{}", interpreter.prompt_raw(), style::Reset);
        stdout.flush().unwrap();
        let input = read_line(&mut stdin).unwrap();
        interpreter.interpret(&input);
    }
}


// TODO: Implement History
// TODO: Move to termion
pub fn read_line<T: TermRead + Read>(term: &mut T) -> Option<String> {
    let mut stdout = stdout().into_raw_mode().unwrap();
    let mut cursor_pos = 0;
    let mut input = "".to_owned();

    let mut iter = term.events();
    let mut err = false;

    loop {
        match iter.next() {
            Some(Ok(ev)) => {
                match ev {
                    Event::Key(Key::Char('\n')) => {
                        print!("\n\r");
                        break;
                    }
                    Event::Key(Key::Char(c)) => {
                        input.insert(cursor_pos, c);
                        print!("{}", input.split_at(cursor_pos).1);
                        cursor_pos += 1;
                        if input.len() - cursor_pos != 0 {
                            print!(
                                "{}",
                                termion::cursor::Left((input.len() - cursor_pos) as u16)
                            );
                        }
                    }
                    Event::Key(Key::Left) => {
                        if cursor_pos > 0 {
                            print!("{}", termion::cursor::Left(1));
                            cursor_pos -= 1;
                        }
                    }
                    Event::Key(Key::Right) => {
                        if cursor_pos < input.len() {
                            print!("{}", termion::cursor::Right(1));
                            cursor_pos += 1;
                        }
                    }
                    Event::Key(Key::Up) => {}
                    Event::Key(Key::Down) => {}
                    Event::Key(Key::Backspace) => {
                        if cursor_pos > 0 {
                            cursor_pos -= 1;
                            input.remove(cursor_pos);
                            print!("{}", termion::cursor::Left(1));
                            print!("{} ", input.split_at(cursor_pos).1);
                            print!(
                                "{}",
                                termion::cursor::Left(((input.len() - cursor_pos) + 1) as u16)
                            );
                        }
                    }
                    Event::Key(Key::Ctrl('c')) => {
                        process::exit(0);
                    }
                    _ => {
                        break;
                    }
                }
            }
            _ => err = true,
        }
        stdout.flush().unwrap();
    }

    if err { None } else { Some(input) }
}
