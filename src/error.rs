use std::path::PathBuf;

use compiler::parser::TokenInfo;
use compiler::parser::grammar::ParseError;
use fileref::FileRef;

/// Keep aware about locate error
#[derive(Clone, Debug)]
pub struct ParserInfo {
    pub line: usize,
    pub col: usize,
    /// Offset to locate error
    pub offset: usize,
    /// Path to source file
    pub path_source: PathBuf,
}

// Cambiar simple por uno con mas opciones
#[derive(Clone, Debug)]
pub enum IError {
    ParserError(ParserInfo, String),
    SimpleError(String),
}

impl IError {
    pub fn from_parser_error(
        parser_error: ParseError,
        fileref: &FileRef,
        comment: Option<String>,
    ) -> IError {
        let mut msg = format!(
            "Expected one of following items: {:?}",
            parser_error.expected
        );
        if let Some(comm) = comment {
            msg.push_str("\nNote: ");
            msg.push_str(&comm);
        }

        IError::ParserError(
            ParserInfo {
                line: parser_error.line,
                col: parser_error.column,
                offset: 0, // TODO:
                path_source: fileref.path_file.clone(),
            },
            msg,
        )
    }

    pub fn simple(message: &str) -> Self {
        IError::SimpleError(message.to_owned())
    }

    pub fn from_token_info(token_info: &TokenInfo, fileref: &FileRef, comment: String) -> Self {
        IError::ParserError(
            ParserInfo {
                line: token_info.line,
                col: token_info.col,
                offset: 0, // TODO:
                path_source: fileref.path_file.clone(),
            },
            comment,
        )
    }

    pub fn pretty_error(&self) -> String {
        match self {
            &IError::SimpleError(ref msg) => msg.to_owned(),
            &IError::ParserError(ref parse_info, ref error) => {
                format!(
                    "\n> {}\n  {:>width$}\n{}",
                    "TODO: Obtener el fuente", //self.source.lines().nth(self.line - 1).unwrap(),
                    "^",
                    error,
                    width = parse_info.col
                )
            }
        }
    }
}
