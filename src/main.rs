#[macro_use]
extern crate clap;
extern crate lang;

use clap::{App, Arg};
use std::path::PathBuf;

use lang::config::Config;
use lang::start;

fn main() {
    let matches = App::new("Lang language")
        .version(crate_version!())
        .author(crate_authors!())
        .about("Does awesome things")
        .arg(
            Arg::with_name("FILE")
                .help("Sets the input file to use")
                .index(1),
        )
        /* TODO: Debug
        .arg(
            Arg::with_name("DEBUG")
                .help("Enable debug mode")
                .short("d")
                .long("debug"),
        )
        */
        .arg(
            Arg::with_name("CONFIG_FILE")
                .help("Set a config file")
                .long("config")
                .short("c")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("VERBOSE")
                .short("v")
                .long("verbose")
                .help("Use mode verbose")
        )
        .get_matches();

    let mut config = matches
        .value_of("CONFIG_FILE")
        .map(|config_file| Config::from_file(PathBuf::from(config_file)))
        .unwrap_or(Config::new());

    start(
        &mut config,
        matches.value_of("FILE"),
        matches.value_of("VERBOSE").is_some(),
    )
}
