/// Define structures to serialize and contrary save hash file
use std::fs;
use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};

use serde_yaml;
use serde_yaml::Value;
use xdg;

use error::IError;

/*
 * TODO: Soportar extension .yml
 */

pub struct ConfigPath {
    pub modules_config_path: PathBuf,
    pub interpreter_config_path: PathBuf,
}

impl ConfigPath {
    pub fn new() -> ConfigPath {
        let mut config_dir = xdg::BaseDirectories::new()
            .expect("Find a XDG Config Directory")
            .get_config_home();
        config_dir.push("lang");
        Self::from_file(config_dir)
    }

    /// Especificar el directorio donde se encuentra la configuración
    pub fn from_file(mut config_dir: PathBuf) -> Self {
        match fs::metadata(&config_dir).map(|m| m.is_dir()) {
            Ok(true) => {}
            Ok(false) => panic!("Expected a dir and found a file"),
            Err(_) => {
                fs::create_dir(&config_dir).expect(&format!(
                    "Create a directory at {}",
                    config_dir.to_str().unwrap(),
                ))
            }
        }
        let mut config_dir_modules = config_dir.clone();
        config_dir_modules.push("modules");
        if !Self::check_config_file(&mut config_dir_modules) {
            Self::generate_default_module_config(&config_dir_modules);
        }

        config_dir.push("lang");
        if !Self::check_config_file(&mut config_dir) {
            Self::generate_default_config(&config_dir);
        }

        Self {
            modules_config_path: config_dir_modules,
            interpreter_config_path: config_dir,
        }
    }

    pub fn check_config_file(path: &mut PathBuf) -> bool {
        path.set_extension("yaml");
        if let Ok(true) = fs::metadata(&path).map(|m| m.is_file()) {
            true
        } else {
            path.set_extension("yml");
            if let Ok(true) = fs::metadata(&path).map(|m| m.is_file()) {
                true
            } else {
                false
            }
        }
    }

    pub fn generate_default_config<P: AsRef<Path>>(path: P) {
        let mut f = File::create(path).expect(&format!("Create the file"));
        write!(f, "{}", include_str!("../lang.yaml")).unwrap();
        f.flush().unwrap();
    }

    pub fn generate_default_module_config<P: AsRef<Path>>(path: P) {
        let mut f = File::create(&path).expect(&format!("Create the file"));
        println!("Gen Module {:?}", path.as_ref());
        write!(f, "{}", include_str!("../modules.yaml")).unwrap();
        f.flush().unwrap();
    }
}


pub struct Config {
    /// Global path to config file
    pub config_path: ConfigPath,
    interpreter_config: Value,
    modules_config: Value,
}


impl Config {
    pub fn new() -> Self {
        let config_path = ConfigPath::new();
        let mut config = Self {
            config_path,
            interpreter_config: Value::Null,
            modules_config: Value::Null,
        };
        let _ = config.load_configs().map_err(|err| {
            println!("{}", err.pretty_error())
        });
        config
    }


    pub fn from_file(path: PathBuf) -> Self {
        let config_path = ConfigPath::from_file(path);
        let mut config = Self {
            config_path,
            interpreter_config: Value::Null,
            modules_config: Value::Null,
        };
        let _ = config.load_configs().map_err(|err| {
            println!("{}", err.pretty_error())
        });
        config
    }

    pub fn load_configs(&mut self) -> Result<(), IError> {
        self.interpreter_config = self.load_config(&self.config_path.interpreter_config_path)?;
        self.modules_config = self.load_config(&self.config_path.modules_config_path)?;
        Ok(())
    }

    pub fn load_config<P: AsRef<Path>>(&self, path: P) -> Result<Value, IError> {
        if let Ok(mut f) = File::open(&path) {
            let mut contents = String::new();
            f.read_to_string(&mut contents).map_err(|err| {
                IError::simple(&format!(
                    "Can't read file: {:?}\n Info: {}",
                    path.as_ref(),
                    err
                ))
            })?;

            serde_yaml::from_str(&contents).map_err(|err| {
                IError::simple(&format!(
                    "Error on parse yaml in file configuration with path: {:?}\n Info: {}",
                    path.as_ref(),
                    err
                ))
            })
        } else {
            Err(IError::simple(&format!(
                "Check permisions to write file: {:?}",
                path.as_ref()
            )))
        }
    }

    pub fn save(&self) -> Result<(), IError> {
        File::open(&self.config_path.modules_config_path)
            .map_err(|_err| IError::simple("Error at open file"))
            .and_then(|file| {
                serde_yaml::to_writer(file, &self.modules_config).map_err(
                    |err| {
                        IError::simple(&format!("Parsing: {}", err))
                    },
                )
            })

    }

    pub fn get_config(&self) -> Value {
        self.interpreter_config.clone()
    }

    pub fn get_module_config(&self) -> Value {
        self.modules_config.clone()
    }

    pub fn set_module_config(&mut self, obj_name: &str, val: Value) {
        self.modules_config[obj_name] = val;
    }
}
