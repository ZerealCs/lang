use std::collections::HashMap;
use std::fmt;

use compiler::layer::intermediate::Instruction;
use objects::RustCall;
use reftable::RefTable;
use serde::{Serialize, Serializer};

/*
 * TODO: definir las conversion a json y al contrario del tipo Value
 */


#[derive(Clone, Debug)]
pub enum Value {
    Nil,
    Boolean(bool),
    Number(i32),
    String(String),
    Vec(Vec<Value>),
    Mapping(HashMap<String, Value>),
    Function(Vec<usize>, Vec<Instruction>),
    Native(RustCall),
}


// TODO
pub struct Iter {}

impl Iterator for Iter {
    type Item = Value;
    fn next(&mut self) -> Option<Self::Item> {
        None
    }
}

impl Value {
    pub fn iter(&self) -> Iter {
        Iter {}
    }

    pub fn get_boolean(&self) -> bool {
        match self {
            &Value::Boolean(b) => b,
            _ => false,
        }
    }

    pub fn call<'a>(
        &self,
        args: Vec<Value>,
        _reftable: &'a mut RefTable<Value>,
    ) -> Result<Value, String> {
        use self::Value;

        match self {
            /*
             * TODO: Fix to reftable
            &Value::Function(ref args_name, ref prog) => {
                env.with_scope(|mut env| {
                    for (val, name) in args.iter().zip(args_name) {
                        env.add_var(name.to_owned(), val.clone());
                    }
                    Expression::evaluate(prog, &FileRef::new(), &mut env)
                        .map_err(|err| err.pretty_error())
                })
            }
            */
            &Value::Native(ref rust_call) => Ok(rust_call(args)),
            _ => Err(format!("{} is not callable object", self.name())),
        }
    }

    /// Access to values properties and methods
    /// TODO: Define basic primitives for values
    pub fn on(&self, attr: &str) -> Option<Value> {
        use self::Value::*;
        match self {
            &Nil => None,
            &Boolean(_) => None,
            &Number(_) => None,
            &String(_) => None,
            &Vec(_) => None,
            &Function(_, _) => None,
            &Native(_) => None,
            &Mapping(ref map) => map.get(attr).map(|val| val.clone()),
        }
    }

    /// Through values with a list of accesor
    /// TODO: Improve error message
    pub fn through(&self, accesor: &[String]) -> Result<Value, String> {
        let mut iter = accesor.iter();
        let mut current_value = self.clone();
        while let Some(ref sub_accesor) = iter.next() {
            match current_value.on(sub_accesor) {
                Some(new_value) => current_value = new_value,
                None => return Err(format!("Error attribute `{}` not found", sub_accesor)),
            }
        }
        Ok(current_value)
    }

    pub fn name(&self) -> &'static str {
        use self::Value::*;
        match self {
            &Nil => "nil",
            &Boolean(_) => "bool",
            &Number(_) => "number",
            &String(_) => "string",
            &Vec(_) => "vector",
            &Function(_, _) => "function",
            &Mapping(_) => "map",
            &Native(_) => "native",
        }
    }
}

impl Serialize for Value {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        use self::Value::*;

        match self {
            &Nil => serializer.serialize_none(),
            &Boolean(b) => serializer.serialize_bool(b),
            &Number(num) => serializer.serialize_i32(num),
            &String(ref string) => serializer.serialize_str(string),
            &Vec(ref values) => Serialize::serialize(values, serializer),
            &Function(_, _) => serializer.serialize_str("[Function]"),
            &Mapping(ref map) => Serialize::serialize(map, serializer),
            &Native(_) => serializer.serialize_str("[Native Function]"),
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Value::Nil => write!(f, "nil"),
            &Value::Boolean(b) => {
                if b {
                    write!(f, "true")
                } else {
                    write!(f, "false")
                }
            }
            &Value::Number(i) => write!(f, "{}", i),
            &Value::String(ref s) => write!(f, "{:?}", s),
            &Value::Vec(ref vals) => {
                write!(f, "[ ")?;
                for val in vals.iter() {
                    write!(f, "{}, ", val)?;
                }
                write!(f, "]")
            }
            // TODO: Show program source
            &Value::Function(ref _program, ref _other) => write!(f, "Function"),
            &Value::Mapping(ref map) => {
                writeln!(f, "{{ ")?;
                for (key, value) in map.iter() {
                    writeln!(f, "  {}: {}", key, value)?;
                }
                write!(f, "}} ")
            }
            &Value::Native(_) => write!(f, "Native"),
        }
    }
}

// TODO: Definir las demas conversiones de primitivas
/*
macro_rules! mk_value{
    ( $($name:ident => $call:expr),* ) => {
        {
            let hashmap = HashMap::new();
            $(hashmap.insert(stringify!($name), mk_value!($call));)*
            Value::Mapping(hashmap)
        }
    };
}
*/
