use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use serde_yaml;

use config::Config;
use error::IError;
use reporter::Reporter;
use value::Value;

// TODO: Generate methods from a file
// TODO: Get documentation to use on interpreter help
pub struct Module {
    pub name: String,
    //state: Value,
    methods: Value,
}

impl Module {
    // TODO:
    pub fn generate_modules(reporter: &Reporter, config: &Config) -> Vec<Module> {
        let empty = vec![];
        config.get_config()["modules"]
            .as_sequence()
            .unwrap_or_else(|| {
                reporter.report_ierror(&IError::simple("modules is not a sequence"));
                &empty
            })
            .iter()
            .fold(Vec::new(), |mut acc, yaml_module| {
                // let Maybe (name, src) = (,) <$> getName yaml <*> getSrc yaml
                let values = yaml_module["name"].as_str().and_then(|name| {
                    yaml_module["src"].as_str().map(|src| (name, src))
                });

                if let Some((name, src)) = values {
                    let mut path = PathBuf::new();
                    path.push(&config.config_path.modules_config_path);
                    path.pop();
                    path.push(src);
                    Module::from_path(reporter, name.to_owned(), path).map(
                        |module| acc.push(module),
                    );
                    acc
                } else {
                    reporter.report_ierror(&IError::simple("module no loaded"));
                    acc
                }
            })
    }


    // TODO
    pub fn from_path(reporter: &Reporter, name: String, path: PathBuf) -> Option<Self> {
        File::open(path.clone())
            .map_err(|err| {
                reporter.report(&format!(
                    "{} ({}) with path `{:?}`",
                    "Error open file",
                    err,
                    path.clone()
                ))
            })
            .and_then(|mut f| {
                let mut contents = String::new();

                f.read_to_string(&mut contents)
                    .map_err(|error| {
                        reporter.report(&format!(
                            "No loaded module `{}`: {:?}",
                            name,
                            error.kind()
                        ));
                    })
                    .and_then(|_| {
                        serde_yaml::from_str(&contents)
                            .map_err(|_err| reporter.report("Parse yaml in file configuration"))
                            .map(|_values: serde_yaml::Value| {
                                // TODO: Generate methods
                                Self {
                                    name,
                                    methods: Value::Nil,
                                }
                            })
                    })
            })
            .ok()
    }

    pub fn save_state(&self) -> serde_yaml::Value {
        serde_yaml::Value::Null
    }
}
