use compiler::ast::ExpressionG;
use compiler::layer::CompilerError;
use compiler::parser::TokenInfo;
use reftable::RefTable;
use reporter::Reporter;
use value::Value;


/// TODO: Maybe it's necesary flat with Value
#[derive(Clone, Debug)]
pub enum ValuePass {
    ByRef(usize),
    ByValue(Value),
}

impl ValuePass {
    pub fn extract(&self, reftable: &mut RefTable<Value>) -> Result<Value, CompilerError> {
        match self {
            &ValuePass::ByValue(ref value) => Ok(value.clone()),
            &ValuePass::ByRef(ref id_ref) => {
                reftable.get_var(*id_ref).ok_or(CompilerError::Error(
                    "It cant be deference".to_owned(),
                ))
            }
        }
    }
}

// TODO: interconect ValuePass with Value
#[derive(Clone, Debug)]
pub enum Instruction {
    // TODO: A little problem with functions into data structure
    Call(usize, Vec<Vec<Instruction>>),
    // Set a new memory section with valuePass, It can be a new value or a reference to other memory
    Assign(usize, Vec<Instruction>),
    // remove a reference to memory
    DropRef(usize),
    // Simple loop controlled by a value (it must be an iterator)
    Loop(Vec<Instruction>, usize, Vec<Instruction>),
    // Simple conditional, ValuePass must be possible to coerce to boolean. Check Value object to see its behavior
    // and conversions.
    If(Vec<Instruction>, Vec<Instruction>, Vec<Instruction>),
    // Lift a value at execution, used to make literals
    Raw(ValuePass),
    // Get from memory a value
    Retrieve(usize),
}

impl Instruction {
    pub fn from_expr(
        expr: ExpressionG<usize, TokenInfo>,
        reporter: &Reporter,
    ) -> Result<Vec<Instruction>, CompilerError> {
        use self::ExpressionG::*;

        match expr {

            SequenceExpr(exprs, _scope_info) => {
                exprs.into_iter().fold(Ok(Vec::new()), |acc, expr| {
                    if let Ok(mut v) = acc {
                        Instruction::from_expr(expr, &reporter).map(|mut instrs| {
                            v.append(&mut instrs);
                            v
                        })
                    } else {
                        acc
                    }
                })
            }

            FunDecl(id_args, body, scope_info) => {
                Instruction::from_expr(*body, &reporter).map(|fun_body| {
                    vec![
                        Instruction::Raw(ValuePass::ByValue(Value::Function(
                                            id_args,
                                            fun_body
                                            // DROP Vars
                                ))),
                    ]
                })
            }

            VarDecl(ref_obj, expr, scope) => {
                Instruction::from_expr(*expr, &reporter).map(|body_expr| {
                    vec![Instruction::Assign(ref_obj, body_expr)]
                })
            }

            // TODO: Don't worry take control with scope and make Apply
            CommandDecl(_command, _token) => unimplemented!(),

            If(cond_expr, body, _token) => {
                Instruction::from_expr(*cond_expr, &reporter).and_then(|cond_inst| {
                    Instruction::from_expr(*body, &reporter).map(|body_inst| {
                        vec![Instruction::If(cond_inst, body_inst, Vec::new())]
                    })
                })
            }

            IfElse(cond_expr, body_true, body_false, _token) => {
                Instruction::from_expr(*cond_expr, &reporter).and_then(|cond_inst| {
                    Instruction::from_expr(*body_true, &reporter).and_then(|body_true_inst| {
                        Instruction::from_expr(*body_false, &reporter).map(|body_false_inst| {
                            vec![Instruction::If(cond_inst, body_true_inst, body_false_inst)]
                        })
                    })
                })
            }

            For(opt_id, expr_iter, body, scope_info) => {
                Instruction::from_expr(*expr_iter, &reporter).and_then(|iter_inst| {
                    Instruction::from_expr(*body, &reporter).and_then(
                        |body_inst| {
                            if let Some(id_ref) = opt_id {
                                Ok(vec![Instruction::Loop(iter_inst, id_ref, body_inst)])
                            } else {
                                unimplemented!()
                            }
                        },
                    )
                })
            }

            Apply(id_call, args_expr, scope_info) => {
                args_expr
                    .into_iter()
                    .fold(Ok(Vec::new()), |acc, expr| if let Ok(mut v) = acc {
                        Instruction::from_expr(expr, &reporter).map(|mut instrs| {
                            v.push(instrs);
                            v
                        })
                    } else {
                        acc
                    })
                    .map(|args_instrs| vec![Instruction::Call(id_call, args_instrs)])
            }

            Identifier(id_name, scope_info) => Ok(vec![Instruction::Retrieve(id_name)]),

            Factor(atom, _scope_info) => {
                Ok(vec![Instruction::Raw(ValuePass::ByValue(atom.to_values()))])
            }
        }
    }

    // TODO: Considerar si es necesario añadir el reporter, ya que un error en ejecucion deberia parar el programa
    // directamente. Buscar algun caso contrario quizas algo dynamic typing
    pub fn step(&self, mut reftable: &mut RefTable<Value>) -> Result<ValuePass, CompilerError> {
        match self {
            &Instruction::Call(ref id_ref, ref args) => {
                reftable.get_var(*id_ref)
                    .ok_or(CompilerError::Error("Var not found".to_owned()))
                    .and_then(|val| {
                        args.into_iter().fold(Ok(Vec::new()), |acc, inst| {
                            acc.and_then(|mut args_v|
                                Instruction::eval(inst, &mut reftable)
                                    // .map_err(CompilerError::Error("Expected to return value".to_owned()))
                                    .and_then(|val| val.extract(&mut reftable))
                                    .map(|new_val|
                                        {args_v.push(new_val); args_v}))
                        })
                            .and_then(|vals|
                                val.call(vals, &mut reftable)
                                    .map_err(|err| CompilerError::Error(err)))
                    })
                    .map(|val| ValuePass::ByValue(val))
            }
            &Instruction::Assign(ref id_var, ref instrs) => {
                Instruction::eval(instrs, &mut reftable)
                    .and_then(|value_pass| {
                        value_pass.extract(&mut reftable).map(|value| {
                            reftable.set_var(*id_var, value)
                        })
                    })
                    .map(|_| ValuePass::ByValue(Value::Nil))
            }
            &Instruction::DropRef(ref id) => {
                reftable.drop_var(*id);
                Ok(ValuePass::ByValue(Value::Nil))
            }
            &Instruction::Loop(ref instrs_iter, ref id_ref, ref loop_body) => {
                Instruction::eval(instrs_iter, &mut reftable).and_then(|iter_value| {
                    let mut result = Ok(ValuePass::ByValue(Value::Nil));
                    for val in iter_value.extract(&mut reftable).into_iter() {
                        reftable.set_var(*id_ref, val);
                        result = Instruction::eval(loop_body, &mut reftable)
                    }
                    result
                })
            }
            &Instruction::If(ref check_instr, ref true_instr, ref false_instr) => {
                Instruction::eval(check_instr, &mut reftable)
                    .and_then(|value_pass| value_pass.extract(&mut reftable))
                    .and_then(|val| if val.get_boolean() {
                        Instruction::eval(true_instr, &mut reftable)
                    } else {
                        Instruction::eval(false_instr, &mut reftable)
                    })
            }

            &Instruction::Raw(ref value_pass) => Ok(value_pass.clone()),

            // TODO:
            &Instruction::Retrieve(ref id_ref) => {
                reftable
                    .get_var(*id_ref)
                    .map(|value| ValuePass::ByValue(value))
                    .ok_or(CompilerError::Error(
                        "Can not find that variable".to_owned(),
                    ))
            }

        }
    }

    pub fn eval(
        instrs: &[Instruction],
        mut reftable: &mut RefTable<Value>,
    ) -> Result<ValuePass, CompilerError> {
        let mut result = Ok(ValuePass::ByValue(Value::Nil));
        for instr in instrs.iter() {
            result = instr.step(&mut reftable);
        }
        result
    }
}

/*
    hello = 10
    func_pretty(hello 45 "fd")

    //////////////////
    intermediate!(
        assign arr *{[1,4,5,7]};
        assign hello *{10};
        call "sum" #hello *{10}
        loop #arr
    )

    let args = get_ref_if_needs(ast_args)  // Se convierte en un Vec<ValuePass>
    intermediate!(
        value <- call "func_pretty" {args};
    )

macro_rules! intermediate {
    () => ();
}

 */
