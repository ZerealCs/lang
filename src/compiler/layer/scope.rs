use std::collections::HashMap;

use compiler::ast::{Expression, ExpressionG};
use compiler::layer::CompilerError;
use compiler::parser::TokenInfo;
use reporter::Reporter;
//use objects::native_funtions;


// TODO: Cambiar el scope Info para obtener el padre
#[derive(Clone, Debug)]
pub struct ScopeInfo {
    //token_info: TokenInfo,
    rename_info: Option<HashMap<String, usize>>,
    last_scope: Option<Box<ScopeInfo>>,
}

impl ScopeInfo {
    pub fn new() -> Self {
        Self {
            rename_info: Some(HashMap::new()),
            last_scope: None,
        }
    }

    fn get_rename_map(&mut self) -> &mut HashMap<String, usize> {
        if let Some(ref mut renamer) = self.rename_info {
            renamer
        } else {
            let mut renamer = HashMap::new();
            self.rename_info = Some(renamer);
            self.get_rename_map()
        }
    }

    /// Generate an identifier to current scope.
    pub fn new_var(&mut self, name_id: String, next: &mut usize) -> usize {
        let aux = *next;
        let renamer = self.get_rename_map();
        *renamer.entry(name_id.clone()).or_insert_with(|| {
            *next += 1;
            aux
        })
    }

    pub fn check_shadownbinding(&self, _name_id: &str) -> Option<CompilerError> {
        None
    }

    /// Search a variable into the current enviroment. It search through the differents scopes. Fail with
    /// info string in case of not found the variable.
    pub fn get_var(&self, name_id: &str) -> Result<usize, CompilerError> {
        if name_id.is_empty() {
            return Err(CompilerError::Critical(
                "Empty name identifier is empty".to_owned(),
            ));
        }

        self.rename_info.as_ref().map_or_else(
            || {
                self.last_scope.as_ref()
                        .map_or_else(
                            || Err(CompilerError::Error("Not found Variable".to_owned())),
                            |scope|
                                scope.get_var(name_id)
                        )
            },
            |renamer| {
                renamer.get(name_id).map_or_else(
                    || {
                        self.last_scope.as_ref()
                                .map_or_else(
                                    || Err(CompilerError::Error("Not found Variable".to_owned())),
                                    |scope|
                                        scope.get_var(name_id)
                                )
                    },
                    |val| Ok(*val),
                )
            },
        )
    }

    pub fn add_scope(&mut self) {}
    pub fn remove_scope(&mut self) {}
    //pub fn get_base_scope(expr: Expression<Rc<ScopeInfo>>) -> ScopeInfo {}
}

// TODO: Se debe incluir el tokeninfo de tal variable para poder ser referenciada en la
// documentacion
pub struct Scope {
    /// id
    pub next: usize,
    pub scope_info: ScopeInfo,
}

impl Scope {
    // TODO: Method to get generate scope
    pub fn new() -> Self {
        Self {
            next: 0,
            scope_info: ScopeInfo::new(),
        }
    }

    // TODO: Add Big Error Reporter
    pub fn scope_transform(
        &mut self,
        expr: Expression<TokenInfo>,
        reporter: &Reporter,
    ) -> Result<ExpressionG<usize, TokenInfo>, CompilerError> {
        use self::ExpressionG::*;
        match expr {
            SequenceExpr(exprs, token) => {
                exprs
                    .iter()
                    .fold(Ok(Vec::new()), |acc, expr| {
                        acc.and_then(|mut vec| {
                            self.scope_transform(expr.clone(), &reporter).map(
                                |expr_trans| {
                                    vec.push(expr_trans);
                                    vec
                                },
                            )
                        })
                    })
                    .map(|new_exprs| SequenceExpr(new_exprs, token))
            }

            FunDecl(args, body, token) => {
                // TODO: No olvidar la captura de variables por parte de la funciones se debe
                // escanear el body de la funcion y avisar de posibles shadownbindings
                self.scope_info.add_scope();
                let new_args = args.iter()
                    .map(|arg| {
                        self.scope_info.check_shadownbinding(arg).map(|error| {
                            // TODO: reporter
                            unimplemented!()
                        });
                        self.scope_info.new_var(arg.to_owned(), &mut self.next)
                    })
                    .collect();
                let result = self.scope_transform(*body, &reporter).map(|scope_body| {
                    FunDecl(new_args, Box::new(scope_body), token)
                });
                self.scope_info.remove_scope();
                result
            }

            VarDecl(name, expr, token) => {
                // // DO NOT make shadownbinding. Needs a special AST (Definition and assign)
                //self.check_shadownbinding(name).map(|error| {
                //    errors.push(error)
                //});

                let new_id = self.scope_info.new_var(name.clone(), &mut self.next);
                self.scope_info.add_scope();
                let result = self.scope_transform(*expr, &reporter).map(|scope_expr| {
                    VarDecl(new_id, Box::new(scope_expr), token)
                });
                self.scope_info.remove_scope();
                result
            }

            CommandDecl(command, token) => Ok(CommandDecl(command, token)),

            If(cond_expr, body, token) => {
                self.scope_info.add_scope();
                let scope_cond_expr = self.scope_transform(*cond_expr, &reporter);
                self.scope_info.remove_scope();
                self.scope_info.add_scope();
                let scope_body = self.scope_transform(*body, &reporter);
                self.scope_info.remove_scope();
                scope_cond_expr.and_then(|new_cond_expr| {
                    scope_body.map(|new_body| {
                        If(Box::new(new_cond_expr), Box::new(new_body), token)
                    })
                })
            }

            IfElse(cond_expr, body_true, body_false, token) => {
                self.scope_info.add_scope();
                let scope_cond_expr = self.scope_transform(*cond_expr, &reporter);
                self.scope_info.remove_scope();
                self.scope_info.add_scope();
                let scope_body_true = self.scope_transform(*body_true, &reporter);
                self.scope_info.remove_scope();
                self.scope_info.add_scope();
                let scope_body_false = self.scope_transform(*body_false, &reporter);
                self.scope_info.remove_scope();
                scope_cond_expr.and_then(|new_cond_expr| {
                    scope_body_true.and_then(|new_body_true| {
                        scope_body_false.map(|new_body_false| {
                            IfElse(
                                Box::new(new_cond_expr),
                                Box::new(new_body_true),
                                Box::new(new_body_false),
                                token,
                            )
                        })
                    })
                })
            }

            For(opt_name, expr_iter, body, token) => {
                self.scope_info.add_scope();
                let new_opt_name = opt_name.clone().map(|name| {
                    self.scope_info.new_var(name.to_owned(), &mut self.next)
                });
                let result = self.scope_transform(*expr_iter, &reporter).and_then(
                    |scope_expr_iter| {
                        self.scope_transform(*body, &reporter).map(|scope_body| {
                            For(
                                new_opt_name,
                                Box::new(scope_expr_iter),
                                Box::new(scope_body),
                                token,
                            )
                        })
                    },
                );
                self.scope_info.remove_scope();
                result
            }

            Apply(name, args_expr, token) => {
                args_expr
                    .into_iter()
                    .fold(Ok(Vec::new()), |acc, arg| {
                        acc.and_then(|mut vec| {
                            self.scope_transform(arg, &reporter).map(|scope_arg| {
                                vec.push(scope_arg);
                                vec
                            })
                        })
                    })
                    .and_then(|args| {
                        self.scope_info.get_var(&name).map(|new_name| {
                            Apply(new_name, args, token)
                        })
                    })
            }

            Identifier(name, token) => {
                self.scope_info.get_var(&name).map(|new_name| {
                    Identifier(new_name, token)
                })
            }

            Factor(atom, token) => Ok(Factor(atom, token)),
        }
    }
}



// TODO: Se tiene que ver que informacion se debe transmitir
//  - Shadownbindings
//  - unused vars
//  - undefined vars
//  - Renaming here -> works over HashSet -> HashMap<String, usize>
//
//  En el caso de las funciones las que obtengan variables del entorno deben registrarlo** y
//  eliminar tal informacion cuando las mismas se eliminen
/*
impl Layer for Scope {
    type InputInfo = TokenInfo;
    type OutputInfo = ScopeInfo<'a>;

    fn transform<'a>(
        &mut self,
        expr: Expression<TokenInfo>,
    ) -> (Expression<ScopeInfo'a>, Vec<(CompilerError, ScopeInfo<'a>)>) {

        unimplemented!()
    }
}
*/

/*
/// Get all identifiers used from program
/// TODO: Remove or move to scope rules layer
pub fn get_used_identifiers(prog: &Vec<Expression<TokenInfo>>) -> Vec<Accesor> {
    use self::Expression::*;
    let mut res = vec![];
    let _ = prog.iter().map(|val| match val {
        &Import(_, _) => {}
        &Module(_, _, _) => {}
        &FunDecl(ref _args, ref prog, _) => {
            res.append(&mut get_used_identifiers(prog));
        }
        &VarDecl(ref nameid, ref expr, _) => {
            res.push(vec![nameid.clone()]);
            res.append(&mut get_used_identifiers(&vec![*expr.clone()]))
        }
        &If(ref expr, ref prog, _) => {
            res.append(&mut get_used_identifiers(&vec![*expr.clone()]));
            res.append(&mut get_used_identifiers(prog));
        }
        &IfElse(ref expr, ref prog_true, ref prog_false, _) => {
            res.append(&mut get_used_identifiers(&vec![*expr.clone()]));
            res.append(&mut get_used_identifiers(prog_true));
            res.append(&mut get_used_identifiers(prog_false));
        }
        &For(ref _nameid, ref iter_expr, ref prog, _) => {
            res.append(&mut get_used_identifiers(&vec![*iter_expr.clone()]));
            res.append(&mut get_used_identifiers(prog));
        }
        &Apply(ref nameid, ref args, _) => {
            res.push(nameid.clone());
            res.append(&mut get_used_identifiers(args));
        }
        &Identifier(ref nameid, _) => res.push(nameid.clone()),
        &CommandDecl(_, _) => {}
        &Factor(_, _) => {}
    });
    res
}
*/
