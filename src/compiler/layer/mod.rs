pub mod intermediate;
pub mod scope;

use compiler::ast::Expression;
use compiler::layer::intermediate::Instruction;
use compiler::layer::scope::Scope;
use compiler::parser::TokenInfo;
use error::IError;
use reporter::Reporter;

/// Indicators of differents types of error on compiling time.
#[derive(Debug)]
pub enum CompilerError {
    /// Stop Compilation and show critical error
    Critical(String),
    /// Keep Compiling but does not run code and show list of errors
    Error(String),
    /// Show warnings, on compilation
    Warning(String),
}

pub fn transform_program(
    program: Expression<TokenInfo>,
    reporter: &Reporter,
    mut scope: &mut Scope,
) -> (Option<Vec<Instruction>>, Vec<(IError)>) {
    // Import modules // make a parse of modules in correspondent order and join like monoid it should produce a initial value for scope state
    // Constant reduction ...
    // TODO: HEre
    match scope.scope_transform(program, &reporter).and_then(
        |scope_expr| {
            Instruction::from_expr(scope_expr, &reporter)
        },
    ) {
        Ok(instrs) => {
            println!("{:?}", instrs);
            (Some(instrs), vec![])
        }
        Err(errors) => {
            println!("{:?}", errors);
            (None, Vec::new())
        }
    }
}

// Check si los identificadores usados estan en ambito
// Hacer una reduccion de expressiones constantes
// Simplificar if despues de la reduccion de expressiones
