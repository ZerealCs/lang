use compiler::ast::{Atom, Expression, Interpreter, Program};
use compiler::ast::ExpressionG::*;
use std::collections::HashMap;

/// Give information about position of token
#[derive(Debug, Clone, Eq)]
pub struct TokenInfo {
    pub line: usize,
    pub col: usize,
    pub offset: usize,
}

impl TokenInfo {
    /// A dummy value used for testing mainly
    pub fn dummy() -> TokenInfo {
        TokenInfo {
            line: 1,
            col: 1,
            offset: 0,
        }
    }
}

#[cfg(not(test))]
impl PartialEq for TokenInfo {
    fn eq(&self, _other: &TokenInfo) -> bool {
        true
        // TODO:
    }
}

// INFO: Used to less verbose testing
#[cfg(test)]
impl PartialEq for TokenInfo {
    fn eq(&self, _other: &TokenInfo) -> bool {
        true
    }
}

/// Enum use to define direction of associativity
#[derive(Debug)]
pub enum Assoc {
    Left,
    Right,
}

/// An internal state of parser, it keeps levels of indentation, and several useful
/// functions used in parsing process to generate final tree.
#[derive(Debug)]
pub struct Internal<'a> {
    /// Relations between indent level and width used
    indent_width: Vec<u8>,
    /// Stack of elements in each indent level
    indent_stack: Vec<Vec<Expression<TokenInfo>>>,
    /// Operators definition
    operators: HashMap<&'static str, (u8, Assoc)>,
    /// Source code
    source: &'a str,
}

impl<'a> Internal<'a> {
    /// Generate a new state with a reference to source
    fn new(source: &'a str) -> Self {
        use self::Assoc::*;
        Internal {
            indent_width: Vec::new(),
            indent_stack: Vec::new(),
            operators: hashmap!{
                "**" => (8, Left),
                "*"  => (7, Left),
                "/"  => (7, Left),
                "%"  => (7, Left),
                "+"  => (6, Left),
                "-"  => (6, Left),
                "++" => (5, Right),
                "==" => (4, Left),
                "!=" => (4, Left),
                "/=" => (4, Left),
                ">"  => (4, Left),
                "<"  => (4, Left),
                "<=" => (4, Left),
                "=>" => (4, Left),
                "&&" => (3, Right),
                "||" => (3, Right),
            },
            source,
        }
    }

    /// Sets a new indentation
    fn new_indent(&mut self, exp: Expression<TokenInfo>, level: u8) {
        self.indent_stack.push(vec![exp]);
        self.indent_width.push(level);
    }

    /// Add an expression to last level of indentation
    fn add_exp(&mut self, expr: Expression<TokenInfo>) {
        let level = self.indent_stack.last_mut().unwrap();
        level.push(expr);
    }

    /// Remove last level and return vec of expressions
    fn pop_level(&mut self) -> Option<Vec<Expression<TokenInfo>>> {
        self.indent_width.pop();
        self.indent_stack.pop()
    }

    /// Last indent chars width
    fn current_width(&self) -> Option<&u8> {
        self.indent_width.last()
    }

    /// Check operator exist and correspond to adequate level
    fn check_operator_level(
        &self,
        operator: String,
        exp: Expression<TokenInfo>,
        level: u8,
    ) -> Result<(String, Expression<TokenInfo>), &'static str> {
        match self.operators.get(operator.as_str()) {
            Some(&(lvl, _)) => {
                if lvl == level {
                    Ok((operator, exp))
                } else {
                    Err("Not correct operator level")
                }
            }
            None => Err("Operator not exist"),
        }
    }

    /// Generate tree from a list expression zipped with operators, using the
    /// association specified by the operator.
    fn tree(
        &self,
        left: Expression<TokenInfo>,
        rest: Vec<(String, Expression<TokenInfo>)>,
    ) -> Expression<TokenInfo> {
        // TODO: Throw error if two diferents association in the same level of precedence
        use self::Assoc::*;

        if let Some(tup) = rest.clone().get(0) {
            match self.operators.get(tup.0.as_str()).unwrap().1 {
                Left => self.right_to_left(left, rest),
                Right => {
                    let (opers, mut expr): (Vec<String>,
                                            Vec<Expression<TokenInfo>>) = rest.into_iter().unzip();
                    expr.insert(0, left);
                    let last = expr.pop().unwrap();
                    self.left_to_right(
                        last,
                        opers
                            .into_iter()
                            .zip(expr.into_iter())
                            .rev()
                            .collect::<Vec<(String, Expression<TokenInfo>)>>(),
                    )
                }
            }
        } else {
            left
        }
    }

    /// Generate a tree with right association
    fn right_to_left(
        &self,
        first: Expression<TokenInfo>,
        rest: Vec<(String, Expression<TokenInfo>)>,
    ) -> Expression<TokenInfo> {
        rest.into_iter().fold(first, |last, (op, exp)| {
            Apply(op, vec![last, exp], TokenInfo::dummy()) // TODO:
        })
    }

    /// Generate a tree with left association
    fn left_to_right(
        &self,
        last: Expression<TokenInfo>,
        rest: Vec<(String, Expression<TokenInfo>)>,
    ) -> Expression<TokenInfo> {
        rest.into_iter().fold(last, |last, (op, exp)| {
            Apply(op, vec![exp, last], TokenInfo::dummy()) // TODO:
        })
    }

    /// Generate TokenInfo from source offset
    pub fn mk_info(&self, _start: usize, _end: usize) -> TokenInfo {
        // TODO:make it
        TokenInfo::dummy()
    }
}


pub mod grammar {
    include!(concat!(env!("OUT_DIR"), "/grammar.rs"));
}

pub fn parse_repl_code(input: &str) -> Result<Interpreter, grammar::ParseError> {
    let mut internal = Internal::new(input);
    grammar::interpreter(input, &mut internal)
}

pub fn parse_file_code(input: &str) -> Result<Program<TokenInfo>, grammar::ParseError> {
    let mut internal = Internal::new(input);
    grammar::program(input, &mut internal)
}

/*
#[cfg(test)]
mod parser_test {
    //trace_macros!(false);
    macro_rules! mk_eq_test {
        ($name:ident, $code:expr, $ast:expr) => {
            #[test]
            fn $name() {
                #[allow(dead_code)]
                use compiler::ast::ExpressionG::*;
                #[allow(unused_imports)]
                use compiler::ast::Atom::*;
                use super::TokenInfo;
                let code:&str = $code;
                let mut internal = super::Internal::new(code);
                assert_eq!(
                    super::grammar::program(code, &mut internal),
                    $ast);
            }
        }
    }

    macro_rules! gen_ast {
        () => (vec![]);
        (prog { $(($($ast:tt)*))* }) => (Ok(vec![ $(gen_ast!($($ast)*)),* ]));
        (fun ($($name:ident),*) { $(($($ast:tt)*))* }) => ({
            FunDecl(vec![$(stringify!($name).to_owned()),*],
                vec![ $(gen_ast!($($ast)*)),* ], TokenInfo::dummy())
        });
        (var $var_name:ident = $($exp:tt)*) => ({
            VarDecl(stringify!($var_name).to_owned(),
                Box::new(gen_ast!($($exp)*)), TokenInfo::dummy())
        });
        (if ($exp:expr) { $( $stmt:expr );* }) => ({
            If(Box::new($exp), vec![ $(gen_ast!($arg)),*], TokenInfo::dummy())
        });
        (if ($exp:expr) { $( $true_stmt:expr );* } else { $( $false_stmt:expr );* }) => ({
            If(Box::new($exp),
                vec![ $(gen_ast!($arg)),*],
                vec![ $(gen_ast!($arg)),* ], TokenInfo::dummy())
        });
        (ap $name:tt [ $( ($($arg:tt)*) ),* ]) => ({
            // TODO: Se debe dividir por puntos el $name
            Apply(vec![$name.to_owned()], vec![ $( gen_ast!($($arg)*) ),* ], TokenInfo::dummy())
        });
            // TODO: Se debe dividir por puntos el $name
        (id $name:tt) => (Identifier(vec![stringify!($name).to_owned()], TokenInfo::dummy()));
        (num $val:expr) => (Factor(Number($val), TokenInfo::dummy()));
        (str $val:expr) => (Factor(String($val), TokenInfo::dummy()));
    }
    /*
    mk_eq_test!{check_right_assoc, "1 ++ 2 ++ 3",
        gen_ast!{
            prog {
                (ap "++" [
                    (num 1),
                    (ap "++" [
                        (num 2),
                        (num 3)
                    ])
                ])
            }
        }
    }


    mk_eq_test!{check_left_assoc, "1 * 2 * 3",
        gen_ast!{
            prog {
                (ap "*" [
                    (ap "*" [
                        (num 1),
                        (num 2)
                    ]),
                    (num 3)
                ])
            }
        }
    }

    mk_eq_test!{check_parens_precedence, "1 * (2 * 3)",
        gen_ast!{
            prog {
                (ap "*" [
                    (num 1),
                    (ap "*" [
                        (num 2),
                        (num 3)
                    ])
                ])
            }
        }
    }

    mk_eq_test!{check_indent, "\
        a1
        a2
        a3
        a4",
        gen_ast!{
            prog {
                (id a1)
                (id a2)
                (id a3)
                (id a4)
            }
        }
    }

    mk_eq_test!{check_variable, "age = oldAge",
        gen_ast!{
            prog {
                (var age = id oldAge)
            }
        }
    }

    mk_eq_test!{check_complex_indent, "\
f1 = fun()
    a1
    a2
    fun()
        b1
        b2
    a3
    fun(arg1
    arg2)
    {
        c1
        c2
    }
    a4",
        gen_ast!{
            prog {
                (var f1 = fun() {
                    (id a1)
                    (id a2)
                    (fun () {
                        (id b1)
                        (id b2)
                    })
                    (id a3)
                    (fun (arg1, arg2) {
                        (id c1)
                        (id c2)
                    })
                    (id a4)
                })
            }
        }
    }
    */
}
*/
