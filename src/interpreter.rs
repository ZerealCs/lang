use std::env;
use std::path::PathBuf;
use std::process;

use compiler::ast;
use compiler::layer::scope::Scope;
use compiler::parser;
use config::Config;
use error::IError;
use fileref::FileRef;
use module::Module;
use prompt::Prompt;
use reftable::RefTable;
use reporter::Reporter;
use value::Value;


const HELP_STR: &'static str = "\

 Commands available from the prompt:

    :help -- Show this help message
    :exit -- Exit from current prompt
";


pub struct Interpreter<'a> {
    reporter: Reporter,
    ///
    modules: Vec<Module>,
    /// Config extracte from configuration files
    config: &'a mut Config,
    /// Prompt configuration
    prompt: Prompt,
    /// Scope generated
    scope: Scope,
    /// Memory used by program
    reftable: RefTable<Value>,
}

impl<'a> Interpreter<'a> {
    /// Initialize a interpreter with configuration given by command or config file.
    pub fn new(config: &'a mut Config, _verbose: bool) -> Self {
        // TODO: Catch exceptions from set_current_dir (invalid path as '~') {{{
        let repl = config.get_config()["repl"]["default_path"].clone();
        if let Some(path) = repl.as_str() {
            let current_path = PathBuf::from(path);
            env::set_current_dir(&current_path).expect("Change current dir");
        }
        // }}}

        let reporter = Reporter::new();
        let prompt = Prompt::new(config);
        let modules = Module::generate_modules(&reporter, config);
        let scope = Scope::new();
        let reftable = RefTable::from_modules(&modules);

        Self {
            reporter,
            modules,
            config,
            prompt,
            scope,
            reftable,
        }
    }

    pub fn interpret(&mut self, input: &str) {
        let fileref = FileRef {
            path_file: PathBuf::from("Interpreter"),
            contents: input.to_owned(),
        };
        match parser::parse_repl_code(input) {
            Ok(ast::Interpreter::Command(name, args)) => self.exec_command(&name, args),
            Ok(ast::Interpreter::Code(prog)) => {
                let (opt_value, errors) = prog.evaluate(
                    &fileref,
                    &mut self.reftable,
                    &mut self.scope,
                    &self.reporter,
                );
                println!("{:?}", self.reftable);
                opt_value.map(|val| println!("{}", val)).unwrap_or_else(
                    || for error in
                        errors.iter()
                    {
                        println!("{}", error.pretty_error())
                    },
                );
            }
            Err(parser_error) => {
                println!(
                    "{}",
                    // TODO: Arreglar el fuente
                    IError::from_parser_error(parser_error, &FileRef::new(), None).pretty_error()
                )
            }
        }
    }

    pub fn compile(&mut self, fileref: &FileRef) {
        parser::parse_file_code(&fileref.contents)
            .map_err(|parser_error| {
                IError::from_parser_error(parser_error, fileref, None)
            })
            .and_then(|prog| {
                let (_opt_value, errors) =
                    prog.evaluate(fileref, &mut self.reftable, &mut self.scope, &self.reporter);
                for error in errors.iter() {
                    println!("{}", error.pretty_error())
                }
                Ok(())
            })
            .unwrap();
    }

    /// Execute an interpreter command with maybe additional args
    pub fn exec_command(&self, name: &str, _args: Vec<String>) {
        let commands_fun = [Self::help, Self::exit];
        let commands_name = ["help", "exit"];
        let mut found = false;
        for (ix, command) in commands_fun.iter().enumerate() {
            if commands_name[ix] == name {
                command(self);
                found = true;
            }
        }
        if !found {
            println!(
                "Command :{} not found, type :help to see a list of available commands",
                name
            )
        }
    }

    // TODO: Auto generar el sistema de ayuda del interprete
    pub fn help(&self) {
        println!("{}", HELP_STR)
    }

    pub fn exit(&self) {
        process::exit(0)
    }

    pub fn prompt_raw(&self) -> String {
        self.prompt.get_prompt(self)
    }
}
