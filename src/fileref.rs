use std::path::PathBuf;

//use error::IError;

/// Estructura intermedia para controlar los archivos abiertos
pub struct FileRef {
    pub path_file: PathBuf,
    pub contents: String,
}

impl FileRef {
    pub fn new() -> Self {
        Self {
            path_file: PathBuf::from("Empty"),
            contents: "".to_owned(),
        }
    }
}
