use super::issue::Issue;
use super::repository::Repository;

pub struct User {}


impl User {
    pub fn new() -> Self {
        Self {}
    }


    pub fn issues(&self) -> Vec<Issue> {
        vec![]
    }

    pub fn repos(&self) -> Vec<Repository> {
        vec![]
    }
}
