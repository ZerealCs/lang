use std::str;
use futures::{Future, Stream};
use tokio_core::reactor::Core;
use hyper_tls::HttpsConnector;
use hyper::{Client, Request, Method, Uri};
use hyper::client::HttpConnector;
use hyper::header::{Scheme, Authorization, Basic, UserAgent, ContentLength, ContentType};
use serde_json::Value;
use serde_json;

pub mod repository;
pub mod user;
pub mod issue;

///
#[derive(Debug)]
pub struct GitHub {
    core: Core,
    client: Client<HttpsConnector<HttpConnector>>,
}


impl GitHub {
    pub fn new() -> Self {
        let core = Core::new().unwrap();
        let client = Client::configure()
            .connector(HttpsConnector::new(1, &core.handle()).unwrap())
            .build(&core.handle());
        GitHub {
            core: core,
            client: client,
        }
    }

    pub fn query(query: &str, token: &str) -> serde_json::Result<Value> {
        let uri = "https://api.github.com/graphql".parse().unwrap();
        let query = format!("{{ \"query\": {} }}", query);
        Self::new().send(&query, format!("Token: {}", token), uri)
    }

    pub fn auth<U: Into<String>, P: Into<String>>(
        username: U,
        password: P,
    ) -> serde_json::Result<Value> {
        let json: &str = r#"{
            "scopes": ["user", "repo", "notifications", "gist"],
            "note": "lang Rust"
        }"#;

        let auth = Basic {
            username: username.into(),
            password: Some(password.into()),
        };
        let uri = "https://api.github.com/authorizations".parse().unwrap();

        Self::new().send(json, auth, uri)
    }

    pub fn send<T>(&mut self, json: &str, auth: T, uri: Uri) -> serde_json::Result<Value>
    where
        T: 'static + Scheme + Clone,
    {
        let mut req = Request::new(Method::Post, uri);
        req.headers_mut().set(UserAgent::new("GHScript"));
        req.headers_mut().set(ContentType::json());
        req.headers_mut().set(ContentLength(json.len() as u64));
        req.set_body(json.to_owned().clone());
        let auth2 = Authorization(auth);
        req.headers_mut().set(auth2);

        let post = self.client.request(req).and_then(
            |res| res.body().concat2(),
        );
        let res = self.core.run(post).unwrap();
        serde_json::from_slice(&res)
    }
}
