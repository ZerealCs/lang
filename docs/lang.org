#+MACRO: lang_name Lang

* {{{lang_name}}} Manual

** Introducción

** Lenguaje {{{lang_name}}}

   {{{lang_name}}} es un lenguaje de tipado dinámico para el desarrollo de /scripts/ para la automatización de tareas con
   con servicios /webs/.

*** Sintáxis

    {{{lang_name}}} se ha diseñado para ser sencillo de usar, para ello se optado por una sintaxis familiar a lenguajes como
    /python/ o /c++/.



** Configuración

   <<file_config>>El fichero de configuración se localiza mediante el XDG
   TODO: Explicar donde busca los ficheros y como trabaja la configuración

** Intérprete
*** Prompt

    En el modo interactivo del intérprete (repl) permite la personalización del *prompt*. Tales como el color del texto,
    salida de la ejecución de comandos shell, y diversos comandos propios del intérprete. La configuración del prompt se
    puede realizar desde el fichero de configuración (véase: [[file_config]]) en la sección *repl*.

    Por defecto, la sección del /prompt/ contiene la siguiente configuración:

#+BEGIN_SRC yaml
repl:
  # ...
  # A portion of code to define repl prompt (Default: "{$pwd} >>>")
  prompt: "{$pwd} >>> "
  # ...
#+end_src

    El prompt permite diferentes añadir texto al mismo desde distintas fuentes. Y realizar un minimo de personalizaciones
    visuales en el color de texto que conforma el prompt.

    - {#.....}
    - {clear_color}
    - {$date YY}
    - {list_modules}

** Arquitectura del proyecto

   El proyecto se encuentra fuertemente inspirado en un proyecto similar realizado en Haskell.

   TODO: continuar
*** Estructura y organización
    TODO: Como esta desarrollado, y cual es el flujo de trabajo
*** Fases del compilador

    El compilador consta de una fase inicial de parsing la cual es realizada con una gramática PEG[[ TODO:Enlace a definición]].
    La cual genera un AST, junto con la información de posición de los tokens que conforman cada elección del árbol.
    Con esta estructura se procede a generar información útil sobre el contexto, tales como el uso de las variables y su scope.
    La mayoría de los errores de compilación se detectan en esta fase de análisis del árbol sintáctico.

    Finalmente se procede a la conversión a una estructura de datos más simplificado orientado a la manipulación de la memoria. El cual
    indicaremos como el lenguaje intermedio.


    relacionado con la gestión de la memoria.


    TODO: Indicar realiza el compilador y cada fase una explicación y enlace al contenido
